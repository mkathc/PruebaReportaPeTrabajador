package com.example.capsula.reportapetrabajadorprueba.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Desarrollo3 on 2/03/2017.
 */

public class ReporteUsuario implements Serializable{
    public String estado;
    public ArrayList<ReporteSemaforo> datos;
    public String mensaje;

    public ReporteUsuario(String estado, ArrayList<ReporteSemaforo> datos, String mensaje) {
        this.estado = estado;
        this.datos = datos;
        this.mensaje = mensaje;
    }
    public ArrayList<ReporteSemaforo> getDatos() {
        return datos;
    }

    public void setDatos(ArrayList<ReporteSemaforo> datos) {
        this.datos = datos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


}
