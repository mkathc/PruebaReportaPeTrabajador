package com.example.capsula.reportapetrabajadorprueba.data.entities;

import java.util.ArrayList;

/**
 * Created by linuxdesarrollo01 on 20/02/17.
 */

public class Reporte {

    public String estado;
    public ArrayList<ReporteGrupal> datos;
    public String mensaje;

    public Reporte(String estado, ArrayList<ReporteGrupal> datos, String mensaje) {
        this.estado = estado;
        this.datos = datos;
        this.mensaje = mensaje;
    }

    public ArrayList<ReporteGrupal> getDatos() {
        return datos;
    }

    public void setDatos(ArrayList<ReporteGrupal> datos) {
        this.datos = datos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }



}
