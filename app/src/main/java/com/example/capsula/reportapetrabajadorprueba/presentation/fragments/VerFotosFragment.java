package com.example.capsula.reportapetrabajadorprueba.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.capsula.reportapetrabajadorprueba.R;
import com.example.capsula.reportapetrabajadorprueba.core.BaseFragment;
import com.example.capsula.reportapetrabajadorprueba.data.entities.ReporteGrupal;
import com.example.capsula.reportapetrabajadorprueba.data.entities.ReporteSemaforo;
import com.example.capsula.reportapetrabajadorprueba.presentation.adapters.VerFotosAdapter;
import com.example.capsula.reportapetrabajadorprueba.presentation.contracts.VerFotosContract;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Desarrollo3 on 2/03/2017.
 */

public class VerFotosFragment extends BaseFragment implements VerFotosContract.View {

    VerFotosAdapter verFotosAdapter;
    GridLayoutManager gridLayoutManager;
    VerFotosContract.Presenter presenter;

    @BindView(R.id.FotoUser)
    RecyclerView FotoUser;


    public static VerFotosFragment newInstance() {
        return new VerFotosFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fotos, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gridLayoutManager = new GridLayoutManager(getContext(),2);
        //gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        FotoUser.setLayoutManager(gridLayoutManager);
       // Bundle bundle = new Bundle();
        //int reporteId = bundle.getInt("reporteId");
        //System.out.println("LLgó id!!!!!!!!!! " + reporteId);
        presenter.start();
    }


    @Override
    public void mostrarFotos(ArrayList<ReporteSemaforo> reporteUser) {
        verFotosAdapter = new VerFotosAdapter(reporteUser, getActivity());
        FotoUser.setAdapter(verFotosAdapter);
    }

    @Override
    public void verFotosDenegado(String response) {

    }


    @Override
    public void setPresenter(VerFotosContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setLoadingIndicator(boolean active) {

    }
}
