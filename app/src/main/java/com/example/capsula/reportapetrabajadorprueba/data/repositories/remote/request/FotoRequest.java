package com.example.capsula.reportapetrabajadorprueba.data.repositories.remote.request;
import com.example.capsula.reportapetrabajadorprueba.data.entities.ReporteUsuario;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Desarrollo3 on 2/03/2017.
 */

public interface FotoRequest {

    @GET("muestraru_porrg/mostrarru/{reportegrupal_id}")
    Call<ReporteUsuario> obtenerFotosReporte(
            @Path("reportegrupal_id") int reportegrupal_id
    );
}
